<?php
/* 
 * Plugin Name: 2024-wp-edit-clothing
 * Description: 米粒 衣服編輯功能, 測試用
 * Version: 1.0.0
 * Author: loulou
 * Text Domain: 2024-wp-edit-clothing
 
 * Requires at least: 6.4
 * Requires PHP: 7.4
 * loulou API Key (2024-06-17 16:24:28)   o94QLTGirgVjJ5iBMq7gsNz7
 */

defined( 'ABSPATH' ) || exit;

class EditClothing {

    public function __construct()
    {
        // 客製 後台 post 表
        add_action("init", array($this, "create_custom_post_type"));
        add_action("init", array($this, "img_upload"));

        // 新增 [前台] assets 腳本(js, css, etc)
        add_action("wp_enqueue_scripts", array($this, "load_assets"));

        // 新增 短代碼 short code
        add_shortcode("edit-clothing", array($this, "load_shortcode"));
    }

    public function create_custom_post_type() {
        $args = array(
            "public" => true,
            "has_archive" => true,
            "supports" => array("title"),
            "exclude_form_search" => true,
            "publicly_queryable" => false,
            "capability" => "manage_options",
            "labels" => array(
                "name" => "Edit Clothing", // 後台標籤名稱
                "singular_name" => "test contact form yo",
            ),
            "menu_icon" => "dashicons-media-text",
        );
        register_post_type("test_contact_form2", $args);
    }
    

    // 新增 [前台] assets 腳本(js, css, etc)
    public function load_assets() {
        // 前台 引入 css
        wp_enqueue_style(
            "2024-wp-edit-clothing",
            plugin_dir_url(__FILE__) . "/css/edit-clothing.css",
            array(),
            1,
            "all"
        );

        // 前台 引入 js
        wp_enqueue_script(            
            "2024-wp-edit-clothing",
            plugin_dir_url(__FILE__) . "/js/move-set-img.js",
            array("jquery"),
            1,
            true
        );
        wp_enqueue_script(            
            "2024-wp-edit-clothing",
            plugin_dir_url(__FILE__) . "/js/upload-set-img.js",
            array("jquery"),
            1,
            true
        );
    }

    public function img_upload() {
        if(isset( $_POST["bwp_upload"])) {
            //echo "<pre>";print_r(($_FILES));"</pre>";
            //echo "<pre>";print_r(($_POST));"</pre>";

            require_once(ABSPATH . "wp-admin" . '/includes/file.php');

            //$file = $_FILES["myfile"];
            $overrides = array( 'test_form' => false);
            $upload_file = wp_handle_upload($_FILES['myfile'], $overrides);
            if(!is_wp_error($upload_file)) {
                //echo "<pre>"; print_r($upload_file);"</pre>";
                update_option("my_upload_image_url", $upload_file["url"]);
            }
            
        }
    }

    // 短代碼 崁入 php
    public function load_shortcode() 
    {?>

        
        <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
        
        
        <div class="edit-clothing-section">

            <h2>Mille Editor</h2>
            
            <div class="edit-clothing-container">
                <div class="edit-clothing-menu">
                    <form name="uploader" method="post" enctype="multipart/form-data">
                        <div class="menu-row">
                            <label for="file-upload" class="btn-primary">
                                選擇檔案
                            </label><span id="file-upload-name">尚未選擇檔案</span>
                            <input id="file-upload" type="file" name="myfile"/>
                            <input type="hidden" name="bwp_upload" value="1">
                            <input type="submit" name="upload_file">
                        </div>  
                        <div class="menu-row">
                            <input id="check-dashed-hide" onclick="clickHideDashed()" type="checkbox"> 隱藏虛線框
                        </div>                      
                    </form>
                    <button onclick="exportToPNG()">匯出成果</button>           
                </div>
                <div class="edit-clothing-content" id="edit-clothing-content">
                    <div class="tab-clothing-type">
                        <button onclick="changeBackgroundClothing('t-shirt')">t-shirt</button>
                        <button onclick="changeBackgroundClothing('polo')">polo</button>
                        <button onclick="changeBackgroundClothing('coat')">coat</button>
                    </div>
                    <div id="app-warpper">
                        <?php

                            $file_url = get_option("my_upload_image_url");
                            if(!empty($file_url)){
                                echo "<img src='" .esc_url($file_url) ."' />";            
                            } 
                            else {
                                echo "<img src='/wordpress/wp-content/plugins/2024-wp-edit-clothing/img/smile.png' />";                                    
                            }

                        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php }

}

new EditClothing;
